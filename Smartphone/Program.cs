﻿using Smartphone.Models;
using System;

namespace Smartphone
{
  class Program
  {
    static void Main(string[] args)
    {
      //this is where your program starts
      //Entry Point

      //instance of smartphone class
      var sc = new SmartphoneClass();

      //set all the properties
      sc.FrontCameraMegapixels = (decimal)1.3;
      sc.Make = "LG";
      sc.Model = "Nexus 5";
      sc.RearCameraMegapixels = 8;
      sc.ScreenHeight = 1920;
      sc.ScreenSizeIn = (decimal)4.95;
      sc.ScreenWidth = 1080;

      //write the phone details out to the console
      Console.WriteLine("**** My Smartphone ****");
      Console.WriteLine();
      Console.WriteLine("Make: {0}, Model: {1}", sc.Make, sc.Model);
      Console.WriteLine("Screen Size: {0} in. ({1} mm)", sc.ScreenSizeIn, sc.ScreenSizeMm);
      Console.WriteLine("Screen Resolution: {0} x {1} pixels", sc.ScreenHeight, sc.ScreenWidth);
      Console.WriteLine("Rear Camera: {0} MP", sc.RearCameraMegapixels);
      Console.WriteLine("Front Camera: {0} MP", sc.FrontCameraMegapixels);
      Console.WriteLine();
      Console.WriteLine("**** Press the AnyKey ****");
      Console.ReadKey();
    }
  }
}
