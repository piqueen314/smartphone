﻿
namespace Smartphone.Models
{
  /// <summary>
  /// our sweet smartphone class
  /// </summary>
  public class SmartphoneClass
  {
    /// <summary>
    /// The make of the smartphone.
    /// </summary>
    public string Make { get; set; }

    /// <summary>
    /// The model of the smartphone.
    /// </summary>
    public string Model { get; set; }

    /// <summary>
    /// Phone's diagonal screen size in inches.
    /// </summary>
    public decimal ScreenSizeIn { get; set; }

    /// <summary>
    /// Read-only screen size in mm.
    /// </summary>
    public decimal ScreenSizeMm
    {
      get
      {
        return ScreenSizeIn * (decimal)25.4;
      }
    }
       
    /// <summary>
    /// Screen width in pixels.
    /// </summary>
    public int ScreenWidth { get; set; }

    /// <summary>
    /// Screen height in pixels.
    /// </summary>
    public int ScreenHeight { get; set; }

    /// <summary>
    /// Front camera megapixels.
    /// </summary>
    public decimal FrontCameraMegapixels { get; set; }

    /// <summary>
    /// Rear camera megapixels.
    /// </summary>
    public decimal RearCameraMegapixels { get; set; }
  }
}
